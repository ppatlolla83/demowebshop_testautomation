package stepdefinitions;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import io.cucumber.java.en.And;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.junit.Assert.assertTrue;

import base.BaseClass;
import pageFactory.HomePageFactory;
import pageFactory.RegisterPageFactory;;

import java.util.UUID;


public class DemoWebShop extends BaseClass {

	WebDriver driver = null;
	HomePageFactory homePage;
	RegisterPageFactory  registerPage;



	public DemoWebShop() throws IOException, InterruptedException {
		driver = super.initializeDriver();
		homePage = new HomePageFactory(driver);
	}

	@Given("User is on Home Page")
	public void user_is_on_home_page() throws InterruptedException {
	}
	
	@Then("Demo Web Shop Image is displayed")
	public void demo_web_shop_image_is_displayed() throws InterruptedException {
		assertTrue(homePage.isDemoWebShopImagePresent());
	}

	@When("User clicks on Register link")
	public void user_clicks_on_register_link() throws InterruptedException {
		homePage.clickOnRegister();
	}

	@Then("Register Page should be displayed title is {string}")
	public void register_page_should_be_displayed_title_is (String title) throws InterruptedException {
		registerPage = new RegisterPageFactory(driver);
		assertEquals(title, registerPage.getTitle());
	}

	@When("User enter mandatory valid values for {string} , {string} , {string} ,{string}, {string}")
	public void user_enter_mandatory_valid_values_for (String firstName, String lastName, String email, String password, String confirmPassword) throws InterruptedException {
		homePage = new HomePageFactory(driver);
		homePage.clickOnRegister();
		registerPage = new RegisterPageFactory(driver);
		registerPage.getFirstNameInput().sendKeys(firstName);
		registerPage.getLastNameInput().sendKeys(lastName);
		// this is logic is to run multiple times this will generate random email.
		String [] email_parts = email.split("@");
		email = email_parts[0]+UUID.randomUUID().toString().substring(0,3)+"@"+email_parts[1];
		registerPage.getEmailInput().sendKeys(email);
		registerPage.getPasswordInput().sendKeys(password);
		registerPage.getConfirmPasswordInput().sendKeys(confirmPassword);
	}

	@And("Clicks on Register Button")
	public void clicks_on_register_button () throws InterruptedException {
		registerPage.clickOnRegisterButton();
	}

	@Then("Registerion should be completed and  acknowledgement message {string} should be displyed")
	public void registerion_should_be_completed_and_acknowledgement_message_should_be_displyed (String acknowledgementMessage) throws InterruptedException {
		assertEquals(acknowledgementMessage, registerPage.getAcknowledgementMessage());
	}

	@After
	public void afterScenario() 
	{
		driver.quit();
	}	
	
	
		
	


}
