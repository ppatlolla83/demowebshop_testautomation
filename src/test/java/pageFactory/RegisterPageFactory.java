
package pageFactory;

        import org.openqa.selenium.JavascriptExecutor;
        import org.openqa.selenium.WebDriver;
        import org.openqa.selenium.WebElement;
        import org.openqa.selenium.support.FindBy;
        import org.openqa.selenium.support.PageFactory;

public class RegisterPageFactory extends BasePage {
    public RegisterPageFactory(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//h1[normalize-space()='Register']")
    WebElement title;

    @FindBy(id = "register-button")
    WebElement registerButton;

    @FindBy(id = "FirstName")
    WebElement firstName;

    @FindBy(id = "LastName")
    WebElement lastName;

    @FindBy(id = "Email")
    WebElement email;

    @FindBy(id = "Password")
    WebElement password;

    @FindBy(id = "ConfirmPassword")
    WebElement confirmPassword;

    @FindBy(xpath = "//div[@class='result']")
    WebElement acknowledgementMessage;

    //div[@class='result']

    public String getTitle(){
        return title.getText();
    }

    public void clickOnRegisterButton(){
        registerButton.click();
    }

    public WebElement getFirstNameInput(){
        return firstName;
    }

    public WebElement getLastNameInput(){
        return lastName;
    }

    public WebElement getEmailInput(){
        return email;
    }

    public WebElement getPasswordInput(){
        return password;
    }
    public WebElement getConfirmPasswordInput(){
        return confirmPassword;
    }

    public String getAcknowledgementMessage(){
        return acknowledgementMessage.getText();
    }



}


