package pageFactory;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePageFactory extends BasePage {
    public HomePageFactory(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//img[contains(@alt,'Tricentis Demo Web Shop')]")
    WebElement ImageFile;

    @FindBy(xpath = "//a[@class='ico-register']")
    WebElement registerLink;




    public Boolean isDemoWebShopImagePresent(){
        Boolean ImagePresent = (Boolean) ((JavascriptExecutor)driver).executeScript("return arguments[0].complete && typeof arguments[0].naturalWidth != \"undefined\" && arguments[0].naturalWidth > 0", ImageFile);
        if (!ImagePresent)
        {
            System.out.println("Image not displayed.");
            return Boolean.FALSE;
        }
        else
        {
            System.out.println("Image displayed.");
            return Boolean.TRUE;
        }
    }

    public void clickOnRegister() {
        if(isElementClickable(registerLink)) {
            registerLink.click();
        }
    }
}

