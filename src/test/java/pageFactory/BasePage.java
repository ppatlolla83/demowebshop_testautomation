package pageFactory;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {

    public WebDriver driver;
    WebDriverWait wait;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    public boolean isElementClickable(WebElement element) {
        return wait.until(ExpectedConditions.elementToBeClickable(element)) != null;
    }

    public boolean isElementClickable(By locator) {
        return wait.until(ExpectedConditions.elementToBeClickable(locator)) != null;
    }

    public boolean isElementVisible(List<WebElement> elements) {
        return wait.until(ExpectedConditions.visibilityOfAllElements(elements)) != null;
    }




}
