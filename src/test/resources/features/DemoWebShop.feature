#Sample Feature Definition Templates
Feature: Automate Demo Web Shop Hige Risk Scenarios 

  @Smoke
  Scenario: Test Home page is displayed
    Given User is on Home Page
	Then Demo Web Shop Image is displayed

  @Smoke
  Scenario: Test Click On Register
    When User clicks on Register link
    Then Register Page should be displayed title is "Register"

  @Smoke
  Scenario Outline: Test User registration is working as expected
    When User enter mandatory valid values for "<first_name>" , "<last_name>" , "<email>" ,"<password>", "<confirm_password>"
    And Clicks on Register Button
    Then Registerion should be completed and  acknowledgement message "Your registration completed" should be displyed
    Examples:
      |first_name 	 	            | last_name 		    | email	                |	password	        | confirm_password	  |
      |customer_1_firstname 		| customer_1_firstname 	| customer1_@gmail.com	|   Cust1_Pwd			| Cust1_Pwd           |
      |customer_2_firstname 		| customer_2_firstname 	| customer2_@gmail.com	|   Cust2_Pwd			| Cust2_Pwd           |